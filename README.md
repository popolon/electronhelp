# Electronhelp

Tools (in Lua) to help electronicians with basic everyday computations.
Outils (en Lua) pour aider les électroniciens dans les calculs de base.

parallel_resistor.lua
* Command line tool to compute parallel resistors total resistance and resulting amperage for a specific voltage.
* Outil en ligne de commande pour calculer les résistance en parallèle et l'ampérage résultant en fonction d'une tension spécifique.

- System language is automatically detected (FR/EN only for now)
- La langue du système est détectée automatiquement (seulement FR/EN pour le moment)

resistorcalc.tic (resistorcalc.lua is extracted source code)
* A GUI tool, using [TIC80](tic80.com) fantasy computer to compute needed resistor for given Vcc, LED color and intensity. Display resistor color bands for 4 and 5 bands resistors (doesn't manage 2 tolerance bands one). I noticed some rounding bug in some rare cases (get ride of it by some tests) 
* Un outil avec GUI utilisant [TIC80](tic80.com) fantasy computer pour calculer la résistance nécessaire pour un Vcc, une couleur de led et une intensité donnée. Affiche les couleurs des bandes pour les résistances à 4 et 5 bandes. ne gère pas les cas de celles utilsiant 2 bandes pour la tolérance. J'ai remarqué quelques erreurs d'arrondis dans certains cas (j'ai compensé par quelques tests supplémentaires).

- use up/down keys to change current active field (or clock with mouse), left/right key to change its value, button 1 (w on QWERTY/z on AZERTY) to change aniamtion
- Utiliser les flèches haut/bas pour changer le champs actif (ou cliquer avec la souris), gauche/droite pour changer ses valeurs, le bouton 1 (w en QWERTY ou z en AZERTY) pour l'animation de la LED.
