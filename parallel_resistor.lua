#!/usr/bin/env lua
read=io.read write=io.write
res={}
U=0
I=0

--print(os.getlocale())

locale=os.getenv("LANG")
os.setlocale(locale)

if string.sub(locale,1,2)=="fr" then
 lang="fr"
else
 lang="en"
end

texts={
en={"resistor","Add a ","Doesn't understand the answer.","for result","Voltage (V)","Rsults with"},
fr={"résistance", "Ajouter une ","Ne comprend pas la réponse.","pour le résultat","Tension (V)","Résultat avec"}
}

function print_all()
 write("U = "..U.." V, ")
 if #res > 1 then
  write(texts[lang][1].."s")
 else
  write(texts[lang][1])
 end
 write(": ")
 for i=1,#res do
  write(res[i]..' Ω')
  if i~=#res then
   write(", ")
  else
   write("\n")
  end
 end
end

answer=false
while(not answer) do
 write(texts[lang][5].." ? ")
 U=tonumber(read("*l"))
 if type(U)=='number' then
  answer=true
 else
  print(texts[lang][3])
 end
end

another=true
while(another) do
 write(texts[lang][2]..texts[lang][1].." (Ω/Ohms) ? (n "..texts[lang][4]..") ") 
 local R = read("*l")
 if type(tonumber(R))=='number' then
  if tonumber(R) == 0 then
   print("Doesn't accept null resistor")
  else
   res[#res+1]=tonumber(R)
   print_all()
  end
 elseif R=='n' then
  if #res==0 then
   print("You need at least one resistor")
  else
   another=false
  end
 else
  print(texts[lang][3])
 end
end

write("Result with: ")
print_all()
print()
write("I = "..U.." * 1 / ( ")
local tmpres=0
for i = 1,#res do
 tmpres=tmpres+(1/res[i])
 write ("1 / "..res[i])
 if i < #res then
  write(" + ")
 end
end
print(" )")
print("  = "..U.." * 1 / "..tmpres.." = "..U.." / "..tmpres)
print("  = "..U.." V * "..(1/tmpres).." Ω")
print("  = "..U*tmpres.." A = "..(1000*U*tmpres).." mA")

